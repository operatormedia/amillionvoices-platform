package org.amillionvoices.config;

import com.twilio.http.TwilioRestClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.config.EnableMongoAuditing;

@Configuration
@EnableAutoConfiguration
@EnableMongoAuditing
public class AppConfig {

    @Value("${twilio.api.sid}")
    private String twilioApiSid;

    @Value("${twilio.api.auth-token}")
    private String twilioApiAuthToken;

    @Bean
    public TwilioRestClient twilioRestClient() {
        return new TwilioRestClient.Builder(twilioApiSid, twilioApiAuthToken).build();
    }
}