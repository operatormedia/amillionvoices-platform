package org.amillionvoices.service.impl;

import org.amillionvoices.domain.model.Search;
import org.amillionvoices.domain.repository.SearchRepository;
import org.amillionvoices.service.SearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SearchServiceImpl implements SearchService {

    @Autowired
    SearchRepository searchRepository;

    @Override
    public Search save(Search search) {
        return searchRepository.save(search);
    }
}