package org.amillionvoices.service;

import org.amillionvoices.domain.model.Search;

public interface SearchService {

    Search save(Search search);
}