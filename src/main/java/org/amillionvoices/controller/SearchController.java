package org.amillionvoices.controller;

import org.amillionvoices.domain.model.Search;
import org.amillionvoices.service.SearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/api/search")
public class SearchController {

    @Autowired
    SearchService searchService;

    @PostMapping
    public Search create(@RequestBody Search search, HttpServletRequest request) {
        search.setUserIp(request.getRemoteAddr());
        return searchService.save(search);
    }
}
