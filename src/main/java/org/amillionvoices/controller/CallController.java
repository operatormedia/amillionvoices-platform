package org.amillionvoices.controller;

import com.twilio.jwt.Jwt;
import com.twilio.jwt.client.ClientCapability;
import com.twilio.jwt.client.OutgoingClientScope;
import com.twilio.jwt.client.Scope;
import com.twilio.twiml.Dial;
import com.twilio.twiml.Number;
import com.twilio.twiml.VoiceResponse;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/call")
public class CallController {

    @Value("${twilio.api.from-number}")
    private String twilioApiFromNumber;

    @Value("${twilio.api.sid}")
    private String sid;

    @Value("${twilio.api.app.sid}")
    private String appSid;

    @Value("${twilio.api.auth-token}")
    private String authToken;

    @PostMapping(produces = MediaType.APPLICATION_XML_VALUE)
    public VoiceResponse create(@RequestParam("To") String to) {
        Dial.Builder dialBuilder = new Dial.Builder().callerId(twilioApiFromNumber);
        dialBuilder.number(new Number.Builder(to).build());

        return new VoiceResponse.Builder().dial(dialBuilder.build()).build();
    }

    @GetMapping("/token")
    public Map<String, String> getToken() {
        List<Scope> scopes = new ArrayList<>();
        scopes.add(new OutgoingClientScope.Builder(appSid).build());

        Jwt jwt = new ClientCapability.Builder(sid, authToken).scopes(scopes).build();

        Map<String, String> rtn = new HashMap<>();
        rtn.put("token", jwt.toJwt());

        return rtn;
    }
}
