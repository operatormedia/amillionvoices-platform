package org.amillionvoices.domain.model;

import org.springframework.data.annotation.*;

import java.util.Date;

public abstract class AbstractMongoDocument {

    @Id
    protected String id;

    @CreatedDate
    protected Date createdDate;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }
}