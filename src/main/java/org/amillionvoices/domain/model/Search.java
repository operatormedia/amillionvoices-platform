package org.amillionvoices.domain.model;

import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Search extends AbstractMongoDocument {

    private double[] loc;
    private String userIp;
    private String formattedAddress;

    public double[] getLoc() {
        return loc;
    }

    public void setLoc(double[] loc) {
        this.loc = loc;
    }

    public String getUserIp() {
        return userIp;
    }

    public void setUserIp(String userIp) {
        this.userIp = userIp;
    }

    public String getFormattedAddress() {
        return formattedAddress;
    }

    public void setFormattedAddress(String formattedAddress) {
        this.formattedAddress = formattedAddress;
    }
}