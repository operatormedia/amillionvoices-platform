package org.amillionvoices.domain.repository;

import org.amillionvoices.domain.model.Search;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface SearchRepository extends MongoRepository<Search, String> {
}